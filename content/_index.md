---
Title: "Home"
menu:
  - main
  - sidebar
weight: -270
---

**2019-02-28:** My next semester at uni begins soon, so posts will likely be
less frequent and shorter than when I started the blog. I hope everyone's 2019
is going well and wish you all the best of luck in work, studies, or other
endevours you're undertaking.

---

Hello there!

You've reached the home of my blog, where I'll be sharing various tidbits, 
ramblings, and miscellaneous updates regarding the various hobbies and 
interests I have. Feel free to have a poke around, and I hope you enjoy your 
stay ^^

\- Trung

