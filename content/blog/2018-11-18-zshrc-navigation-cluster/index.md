---
title: ".zshrc: getting the navigation cluster working"
description: "How to get arrow keys/Home/End/Del/etc. working in Zsh"
date: 2018-11-18T13:10:00+11:00
cover:
  image: "laptop-keyboard-crop.jpg"
  alternate: "Close up photo of a laptop keyboard from the side."
  caption:
    "Photo \
    by [Manideep Karne](https://unsplash.com/photos/r76523xsOZY) \
    on [Unsplash](https://unsplash.com/)"
  style: normal
categories:
  - dotfiles
tags:
  - dotfiles
  - linux
  - shell
  - terminal
  - zsh
---

I currently use Zsh as my shell along with urxvt for my terminal emulator. With
the default Zsh bindings, my navigation cluster keys (i.e. arrow keys +
home/end/del/etc.) weren't working like they did in Bash. This is because when
I hit those keys, the codes sent by urxvt to Zsh aren't the same as those
defined in Zsh's default keybindings.[^bindkey]

To fix these, I needed to bind the character sequence sent by urxvt (which can
be found by typing ctrl+v followed by the appropriate key). You can see how
I've done this in the [relevant section of my .zshrc][zshrc].

Rather than hardcoding the codes themselves (which would likely change if I
switch terminal emulators), I queried the values from`$terminfo`. For these
values to be accurate, the terminal [must be in application mode][f30 post]:

```zsh
if (( ${+terminfo[smkx]} )) && (( ${+terminfo[rmkx]} )); then
	function zle-line-init() {
		echoti smkx
	}
	function zle-line-finish() {
		echoti rmkx
	}
	zle -N zle-line-init
	zle -N zle-line-finish
fi
```

From there, it's just a matter of finding the right codes for `$terminfo` and
commands for ZLE[^zle]:

| Key       | `$terminfo` code | ZLE Command             |
|-----------|------------------|-------------------------|
| Page Up   | kpp              | up-line-or-history      |
| Page Down | knp              | down-line-or-history    |
| Up        | kcuu1            | up-line-or-search       |
| Down      | kcud1            | down-line-or-search     |
| Home      | khome            | beginning-of-line       |
| End       | kend             | end-of-line             |
| Delete    | kdch1            | delete-char             |

From there the keybinds can be added to .zshrc like so:  
`bindkey "${terminfo[kpp]}" up-line-or-history`  
Unfortunately, the terminfo databases do not contain anything for the
ctrl+left/right combinations, which means their codes must be defined manually.
Using ctrl+v gives `^[Oc` and `^[Od` on my setup.

Some configuration frameworks for Zsh (such as [pretzo] and [oh-my-zsh]) set
all this up out of the box, so that's also an option if you'd prefer. Having a
look at Pretzo's configuration in particular, it appears to bind four key
sequences each for ctrl+left/right to cover as many bases as possible. I may
implement this in my own config at some point, but I'm currently happy with my
Zsh config as is.

[^bindkey]: use `bindkey` without any arguments to see current keybinds
[^zle]: zle = [zsh line editor](http://zsh.sourceforge.net/doc/release/zsh-line-editor.html)

[zshrc]: https://gitlab.com/driftingmuse/dotfiles/blob/32b56560dfa75cf50e25f732a0d72c04b722f9bd/dotfiles/zshrc#l30
[f30 post]: http://www.f30.me/2012/10/oh-my-zsh-key-bindings-on-ubuntu-12-10/
[pretzo]: https://github.com/sorin-ionescu/prezto
[oh-my-zsh]: https://ohmyz.sh/

