---
title: "What's in a name?"
description: "Why and how I came up with the name for this blog"
date: 2018-11-21T12:20:34+11:00
cover:
  image: "red-rose-black-bg-crop.jpg"
  alternate: "Close up photo of a red rose."
  caption:
    "Photo \
    by [Carlos Quintero](https://unsplash.com/photos/Y2G16TB2fws) \
    on [Unsplash](https://unsplash.com/)"
  style: normal
categories:
  - blog
tags:
  - a drifting muse
  - blog
  - interests
  - literature
  - meta
  - muses
---

> What's in a name? That which we call a rose  
> By any other word would smell as sweet;  

> — Romeo and Juliet II.ii.43-4

The name "drifting muse" is what I'm using as a username for stuff I publish
and put up online. The big reasons for this are:

 1. My name is common enough that it would be nearly impossible to get a simple
    variation of it as a username without adding numbers (which I always want
    to avoid)
 2. I want the name to describe myself in some way - my real name doesn't do
    this unless you already know me personally.

So what does "drifting muse" mean, anyway? In Greek Mythology [the Muses][wiki]
are the "inspirational goddesses of literature, science, and the arts", and are
considered the source of the knowledge in those things.  Whilst I won't ever
claim to be any sort of goddess, many of my interests lie within the
disciplines represented by the Muses. Additionally, I've recently noticed that
I tend to shift my focus between hobbies.that's where the "drifting" part comes
from.

With those two elements, I have a name which I feel represents an aspect of me.
Its grammatical form (present participle used as an adjective[^alt] followed by
a noun) was likely inspired by John Gruber's blog, [Daring Fireball], which I
think rings quite neatly :)

[^alt]: sometimes called a [participial adjective](https://www.thoughtco.com/what-is-a-participial-adjective-1691486)

[wiki]: https://en.wikipedia.org/wiki/Muses
[daring fireball]: https://daringfireball.net/
