---
title: "…and we are live!"
date: 2018-11-14T19:53:37+11:00
cover:
  image: "paper-boat-water-crop.jpg"
  alternate: "Paper boat floating in water"
  caption:
    "Photo \
    by [Ahmed zayan](https://unsplash.com/photos/sRg9N_0pn1Q) \
    on [Unsplash](https://unsplash.com/)"
categories:
  - blog
tags:
  - a drifting muse
  - blog
  - interests
  - meta
  - updates
---

Hello!

Welcome to my blog! This is where I'll be posting about my hobbies, interests,
and any other things I feel like sharing. These will include (among many
others) technology, music, video games, and origami. Basically, this blog will
be about myself and some of the stuff I get up to, and to reflect that I've
chosen the moniker "drifting muse". I'll post more about how I came up with the
name itself at some point.

I've got a few ideas in the pipeline for my first couple of posts. Starting off
with some tech stuff amongst other topics, I'll be commenting on my experiences
in setting up with blog and the things that have made it possible. First, I'd
like to give a shoutout to the people who gave me the idea to make a blog, and
encouraged me to go through with the process. Without them, this blog would
still be sitting in my list of "things to do… eventually" (and unlikely to have
made it out).

As I add more content to this blog, I only hope that for those of you reading
along it can be something useful, interesting, and/or just a nice way to pass
time.  Only time will tell where this little venture in my spare time will end
up, and I'll be glad to have you along for the ride.

