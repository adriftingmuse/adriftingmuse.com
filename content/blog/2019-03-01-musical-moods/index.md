---
title: "Musical Moods"
description: "What I'm listening to this week"
date: 2019-03-01T09:15:07+11:00
categories:
  - music
tags:
  - interests
  - music
  - guitar
  - gipsy kings
  - updates
---

It's March already! This year's coming along pretty quickly for me, and I'm perfectly fine with that. Hope you're having as nice of a year as I am so far.

I listen to a modest variety of music, including genres such as jazz,
classical[^period], electroswing, bossa nova, salsa, dubstep, *lofi hip hop
radio - beats to relax/study to*[^lofi], and various soundtracks from games
I've played. I don't listen to all of these at the same time, the biggest
reason being that my mood often dictates what I'll put on. As much as I love
the wubs and dubs, I'll never play dubstep if I'm just looking to chill.

This past week I've really been vibing [Gipsy Kings] \[[Youtube Topic]]
\[[Spotify]]. I listened to some of their music growing up, and I'm a huge fan
of their style of guitar playing (which has roots in flamenco). I just love the
energy and groove in their music.

For more guitar-y musical goodness, also check out [Los Hijos de Frida]

[^period]: "classical" meaning in general, not just the classical period
[^lofi]: I'm fully aware that the genre is just "lo-fi hip hop", but I associate it so strongly with the radio streams on Youtube. My current go-to ones are by ChilledCow.


[gipsy kings]: https://en.wikipedia.org/wiki/Gipsy_Kings
[youtube topic]: https://www.youtube.com/channel/UCr9D3wnMkOrlJvwW0UAoyQQ/
[spotify]: https://open.spotify.com/artist/3jc496ljiyrS3ECrD7QiqL
[los hijos de frida]: https://open.spotify.com/artist/5mxd434NfzT0CjR4LZSPrA
