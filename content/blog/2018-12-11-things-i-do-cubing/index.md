---
title: "Things I do: cubing"
date: 2018-12-10T10:50:27+11:00
categories:
  - cubing
tags:
  - interests
  - puzzles
---

Cubing? Isn't that a maths thing? Yes, but it's probably not what I'm referring
to.

Speedcubing, or "cubing" for short, is the act of solving twisty puzzles as
quickly as possible. Twisty puzzles include the [Pyraminx], [Square-1], and all
the cubes of various sizes - the most well-known of these being the 3x3x3
Rubik's cube.

I often bring my 3x3 along in my backpack to help me pass the time, especially
on public transport. Friends have often asked how long I've been solving. The
answer can be either "8+ years", or "a few months" depending on how you look at
it:

+ **sometime before 2010**: learned a basic method and played around with it
  for a few months. I got my times down to ~5 minutes before I stopped solving
  for the next couple of years.
+ **2017 Jan-Mar**: got a proper speedcube, tried to learn [Heise], gave up on
  that and learned [Roux] instead, stopped again.
+ **2018 Oct-present**: picked up cubing again

So, a few months of cubing spread across nearly a decade.

---

To elaborate, I first learned how to solve some time in primary school when I
was gifted a cheap cube by a relative. I borrowed [this book][depository] from
my local library and learned the its basic layer-by-layer method. This is quite
similar to most other beginner methods you'll find, with one key being that it
solves corners first instead of the cross. Whilst I would now recommend other
guides for beginners[^why] \(with plenty [freely][feliks] available
[online][cubicle]), the other sections of the book which went into the history
of puzzles before/after the cube, and of the the cube itself, are quite
interesting reads and provide a good glimpse into the word of twisty puzzles.
During this time I also obtained a Rubik's brand 4x4, and another 3x3 from the
Scienceworks store; neither puzzle turned decently, but I didn't know any
better at this point

In the years after that, I barely touched a cube until early 2017. I'm not
quite sure why, but it's likely that other interests merely drew my attention
away from cubing. Early in 2017, I discovered that I could get a decent
speedcube quite cheaply, and picked up the MF3RS for 7.50 AUD (along with a 5x5
and pyraminx). The difference in turning quality was massive -  many turns
could now be done as flicks instead of requiring my whole wrist and the ability to
corner cut[^align] meant *much* less catching during a solve. Solving became a
much more enjoyable experience.

At this point I really liked the idea of being able to solve the cube with
intuition alone - i.e. without any algorithms, which most speedsolving methods
employ towards the end. I tried to learn the [Heise] method for solving but this
didn't go too well, so I relented and decided to give other methods a chance.

I began learning the [Roux] method for solving; Roux is one of the more popular
methods for speedsolving, and involves M slice moves which I find quite fun. At
this time I also started using [Twisty Timer] to time my solves; my initial
times were around the 5 minute mark, but over the next three months I lowered
my times to around 1:30. Some time around March I took another break from
cubing, partly because it was my final year of secondary school and I'd become
increasingly busy.

Upon picking it up again this year, I had to relearn the algorithms (most of
which I'd forgotten by then) and regain my speed. Fortunately, progress was
faster the second time around, and I've since broken the sub-minute mark. My
current goal is to eventually reach sub-30, and from there see how far I can
take things. I'm also looking to obtain some more twisty puzzles - I'll share
that here once it happens :)

---

NB 2018-12-11: I normally try and break up the text in my posts with some
images or photos, but I want to figure out how to use Hugo's image processing
features first - I'll update this post when I do.

[^why]: the guides I linked start with a cross, which makes it slightly easier to advance to CFOP, and teach the standard notation in which most algorithms are written and shared.
[^align]: corner cutting is a puzzle's ability to turn a face whilst another is misaligned.


[pyraminx]: https://en.wikipedia.org/wiki/Pyraminx
[square-1]: https://en.wikipedia.org/wiki/Square-1_(puzzle)
[heise]: https://www.speedsolving.com/wiki/index.php/Heise_method
[roux]: https://www.speedsolving.com/wiki/index.php/Roux_method
[feliks]: https://www.cubeskills.com/tutorials/the-beginners-method-for-solving-the-rubiks-cube
[cubicle]: https://www.youtube.com/watch?v=1t1OL2zN0LQ
[depository]: https://www.bookdepository.com/Cube-Geert-Hellings/9781579128050
[twisty timer]: https://play.google.com/store/apps/details?id=com.aricneto.twistytimer

