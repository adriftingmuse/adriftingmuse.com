---
title: "DnD Session 0: I am confusion"
description: "or why I will never complain about long character creation in video games again"
date: 2018-11-29T18:44:40+11:00
categories:
  - Dungeons & Dragons
tags:
  - dnd
  - dungeons & dragons
  - role playing games
---

DnD is something I'd been wanting to play for a while. Key issues stopping me
were finding time to run regular sessions, and getting a group together. These
were both recently solved by my end of year break giving me the time and one of
my good friends making the group (cheers, Matt!). With a basic understanding of
how DnD works, five of us met today to begin our DnD adventures… kinda.

A proper beginning to a campaign would typically require that the Dungeon
Master (DM) prepares the campaign, and that the players have filled character
sheets. For us first-time players, however, we decided to meet up just with the
intention of learning the basics and creating our first character sheets.
Equipped with a copy of the Player's Handbook and the free Basic Rules
(available as [PDF] or [webpage]), we began working through our
character sheets together.

Progress was slow and steady as we slowly followed the steps to creating a
character. Switching back and forth between the relevant parts of the text
(e.g. to look up class information for a step) was quite time consuming as we
often didn't know where to find the relevant information at first, and were
sharing 2 laptops amongst the five of us. The example/pre-generated sheets were
quite helpful for seeing where and how all the information should be entered.
By the end of the "session", we hadn't quite managed to finish the sheets - I
still need to add the starting spells and back story - but I think we've
definitely overcome a decent portion of the initial learning curve to getting
started.

I've got high hopes that things will only get easier from here (and definitely
so for subsequent campaigns), and I'm really looking forward to when we do
start the campaign proper. Watch this space for - if all goes well, I'll do my
best to write about whatever shenanigans we get up to in-game.

[pdf]: http://dnd.wizards.com/articles/features/basicrules?x=dnd/basicrules
[webpage]: http://dnd.wizards.com/products/tabletop-games/trpg-resources
[character sheets]: http://dnd.wizards.com/articles/features/character_sheets

