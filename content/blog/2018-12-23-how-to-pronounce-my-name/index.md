---
title: "How to pronounce my name"
description: "and other name-related shenanigans"
date: 2018-12-23T16:00:45+11:00
cover:
  image: "common-names-cloud.png"
  alternate: 'Word cloud of common australian names'
  style: normal
categories:
  - blog
tags:
  - a drifting muse
  - pronunciation
  - names
---

It's pretty common for children and further descendants of immigrants in
Australia to be given English names. My parents, however, were particularly
keen on my remembering my background/origin and thus gave me a Vietnamese name
- Trung. My younger self thought that name wasn't that cool and didn't like it 
that much, but I've since grown to like it more. Although Trung is a somewhat
common name (I've met at least four other Trungs in my lifetime), I still
consider it more of a unique name than say Matthew (of which there are *three*
amongst my friends, and many others elsewhere).

Most people who read my name before saying it rhyme it with "strung" or "rung".
Whilst this is incorrect (considering both the proper pronunciation as well as
its "English" approximation), it's understandable since it's what I'd say
myself when trying to sound out the letters phonetically in English[^english].
[Wikitionary] lists pronunciations (for different Vietnamese dialects) using
the International Phonetic Alphabet. For peeps like myself who can't quite read
the IPA, an easier way could be to pronounce my name as if it's spelt "Truong"
(which is how people who hear my name first try to spell my name).

---

As far as non-English names goes it's far from the most difficult of names out
there, but I often find myself repeating it two or three times to others. This
is fine if I'm introducing myself to someone, but it's a clumsiness I'd rather
avoid if I'm ordering coffee/food and the cashier asks for my name. Thus, I've
gotten into the habit of making up an "easier" name to give. To keep things
more interesting, I switch up the names each time: one day I'm 'Bob', the next
week I'm 'Alex'; 'Jake' might order the bubble tea, but it's 'John' who got a
cappucino at the cafe down the road. By using a name other than my own (which I
would automatically react to), I have to listen a bit more carefully to make
sure I catch it, but I consider the trade off worthwhile for a smoother
checkout.

I also have a middle name, which I quite like except when websites/applications
mess it up by thinking my middle name is part of my first name. This tends to
happen when they've grabbed my info from a source that only separates my name
into given and surname. One occurance that really irked me was when I was
trying to verify my identity for my [Australia Post] account - it kept
rejecting the ID I put it, and it was only after I deliberately entered my
middle name into the wrong field that it finally stopped complaining.

Names are hard - even computers can get them wrong - so don't feel too bad
about messing up a name once or twice.

[^english]: though we all know how unreliable that can be in the mashup of a language that is *English*.


[wikitionary]: https://en.wiktionary.org/wiki/trung#Pronunciation
[australia post]: https://auspost.com.au/

