---
title: "Melbourne Night Noodle Markets"
description: "tl;dr: stomach full, wallet empty"
date: 2018-11-25T13:24:09+11:00
cover:
  image: "night-noodle-markets-gate.jpg"
  alternate: 'Torii with lanterns and a neon sign: "Night Noodle Markets"'
  style: normal
categories:
  - blog
tags:
  - blog
  - events
  - food
  - melbourne
  - noodles
---

Yesterday, I caught up with some high school friends and together we checked
out the [Melbourne Night Noodle Markets][website]. Although it was raining
lightly earlier in the afternoon, things had already cleared up by the time we
made our way there. Skies were relatively clear for the whole evening/night :D

From the name, I was expecting something more like a street market full of
stalls to get small bites of food from. Whilst there were plenty of stalls to
check out, many of the items were more like small meals/sides which made it
difficult to try out any more than handful of items unless you had a massive
appetite.

{{<figure src="hoy-pinoy-smoke.jpg"
  alt="Hoy Pinoy stall, producing plenty of smoke"
  caption="*The Hoy Pinoy stall - hard to miss from all that smoke*"
>}}

The event was held at Birrarung Marr, which was walking distance from Flinder
St station. This made it quite convenient to get to by both train and tram.  We
took the tram from Melbourne uni, where most of us had met up for the day.
There was plenty of space to walk around, and thankfully most of the queues
were short and/or moved quickly.

The morning rain and foot traffic of the previous weeks of the event made parts
of the upper and middle terraces muddy.  Plastic panels had been put down to
make some paths over the mud but there were still some stalls you had to cross
wet ground to reach - not ideal, but not a too big of an issue unless you wore
wore open-toe footwear or sprinted all over the place.

{{<figure src="chicken-nuggets-and-coconut.jpg"
  alt="Buttermilk chicken nuggets and coconut"
  caption="*The splurge begins, featuring a most aesthetic mayo blob*"
>}}

First items I bought yesterday were the sake buttermilk chicken nuggets from
the Sash Japanese stall, as well as a coconut from the drinks stall. These were
very nice, but I was still hungry so I ordered some pad thai chicken from
Shallot Thai later on in the evening. Craving some sweet stuff and following a
friend's recommendation, I dropped by the Gelato Messina stall and got "The
Jolli P" - a desert with peach and mango flavours. I also would have liked to
have tried something from Black Star Pastry, but by this point I was properly
full and wouldn't have been able to finish it, let alone enjoy it :P

Everything was card only, which definitely did help with queue times.  All the
items were quite pricey (see table below), and I was only willing the spend as
much as I did because of the occasion and becauseI was trying out some new
foods [^exception].

| Yummy stuff :D             | How much emptier my bank account got :'( |
|:---------------------------|-----------------------------------------:|
| nuggets                    | $14.00                                   |
| coconut                    |  $7.50                                   |
| pad thai                   | $15.00                                   |
| jolli p                    | $12.00                                   |
| **total damage to wallet** | **$48.50**                               |

All in all, yesterday was a very fun day - especially catching up with some
friends I hadn't seen in a while. I really enjoyed checking out the different
cruisines on off, and I'll most likely check out the Night Noodle Markets again
when they return next year.

[^exception]: the coconut was one exception - hadn't had it for a while and was thirsty for a nice drink

[website]: https://www.goodfoodmonth.com/melbourne/night-noodle-markets/

